$(document).ready(function () {
  // часть, которая должна быть всегда
  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  // /маска для инаупов
  // раворачивание поиска
  if ($(".js-head-search").length) {
    $(".js-head-search__show").on("click", function () {
      $(".js-head-search").slideToggle();
    });
  }
  // раворачивание поиска
  // всплывашка на видео
  if ($(".js-video").length) {
    $(".js-video").fancybox();
  }
  // /всплывашка на видео
  // красивый select
  if ($(".js-select2").length) {
    $(".js-select2").select2({
      escapeMarkup: function (text) {
        if (text.indexOf("~") > 0) {
          text = text.split("~");
          return (
            '<span class="select-name">' +
            text[0] +
            '</span><span class="select-count">' +
            text[1] +
            "</span>"
          );
        }
        return text;
      },
    });
  }
  // /красивый select
  // закрытие попапа при клике на кнопку
  $('.popup a[href*="#"]').click(function () {
    console.log(11);
    $.fancybox.close();
  });
  // /закрытие попапа при клике на кнопку
  // декорация инпута для файла
  $(".js-file-input").change(function () {
    $(".js-file-custom").text($(".js-file-input")[0].files[0].name);
  });
  // /декорация инпута для файла
  // плавный скролл
  $(".js-scroll").on("click", "a", function (event) {
    event.preventDefault();
    var id = $(this).attr("href"),
      top = $(id).offset().top;
    $("body,html").animate({ scrollTop: top }, 1500);
  });
  // /плавный скролл
  //  /часть, которая должна быть всегда

  // адаптивное меню
  $(".js-adaptive-menu__show").on("click", function () {
    $(this).toggle();
    $(".header").toggleClass("adaptive");
    $(".js-adaptive-menu__hide").toggle();
    $(".js-adaptive-menu").slideToggle();
  });
  $(".js-adaptive-menu__hide").on("click", function () {
    $(this).toggle();
    $(".header").toggleClass("adaptive");
    $(".js-adaptive-menu__show").toggle();
    $(".js-adaptive-menu").slideToggle();
  });

  $(".js-main-menu__show-child").on("click", function () {
    $(this).parents(".js-toggle-main-menu").toggleClass("active");
    $(this)
      .parents(".js-toggle-main-menu")
      .find(".js-main-menu-level2")
      .animate({
        width: "toggle",
      });
    $(this)
      .parents(".js-toggle-main-menu")
      .find(".js-main-menu-level2")
      .toggleClass("active");
  });
  $(".js-main-menu__show-child2").on("click", function () {
    $(this).parents(".js-main-menu__item2").toggleClass("active");
    $(this)
      .parents(".js-main-menu__item2")
      .find(".js-main-menu-level3")
      .animate({
        width: "toggle",
      });
    $(this)
      .parents(".js-main-menu__item2")
      .find(".js-main-menu-level3")
      .toggleClass("active");
  });
  if ($(window).width() < 767) {
    $(".js-head-topmenu-level2").hide();
  }
  $(".js-head-topmenu__show").on("click", function () {
    $(this).toggleClass("active");
    $(this)
      .parents(".head-topmenu__item")
      .children(".js-head-topmenu-level2")
      .slideToggle();
  });
  // /адаптивное меню
  // меню в подвале
  $(".js-toggle-foot-menu-show").on("click", function () {
    $(this).toggleClass("active");
    $(this).parents(".foot-menu__col").children(".js-foot-menu").slideToggle();
  });

  $(".js-toggle-foot-contact-show").on("click", function () {
    $(this).toggleClass("active");
    $(this)
      .parents(".js-foot-contact")
      .children(".js-foot-contact-content")
      .slideToggle();
  });
  // /меню в подвале

  // главное меню
  if ($(window).width() < 1240 && $(window).width() > 767) {
    $(".js-toggle-main-menu").on("click", function (e) {
      e.preventDefault();
      $(this).children("a").toggleClass("active");
      $(this).children(".js-main-menu-level2").toggleClass("active");
    });
  }
  // /главное меню
  // слайдер на главной
  if ($(".js-slider").length) {
    var swiper = new Swiper(".js-slider", {
      navigation: {
        nextEl: ".slider-button-next",
        prevEl: ".slider-button-prev",
      },
      pagination: {
        el: ".slider-pagination",
        dynamicBullets: true,
      },
    });
  }
  // /слайдер на главной
  // разводка категорий
  if ($(".js-category").length) {
    if ($(window).width() < 767) {
      $(".js-category").addClass("swiper-container");
      $(".js-category").wrapInner("<div class='swiper-wrapper'></div>");
      $(".js-category__item").addClass("swiper-slide");
    }
    var swiper = new Swiper(".js-category", {
      slidesPerView: 2,
      spaceBetween: 10,
    });
  }
  // /разводка категорий
  // Хиты продаж
  if ($(".js-catalog-tabs").length) {
    $(".js-catalog-tabs").each(function () {
      let ths = $(this);
      ths.find(".js-catalog-tabs__item").not(":first").hide();
      ths
        .find(".js-catalog-tabs__tab")
        .click(function () {
          ths
            .find(".js-catalog-tabs__tab")
            .removeClass("active")
            .eq($(this).index())
            .addClass("active");
          ths
            .find(".js-catalog-tabs__item")
            .hide()
            .eq($(this).index())
            .fadeIn();
        })
        .eq(0)
        .addClass("active");
    });
  }
  // /Хиты продаж
  // Сортировка каталога
  if ($(".js-catalog-sort").length) {
    $(".js-catalog-sort").each(function () {
      let ths = $(this);
      ths.find(".js-catalog-sort__content").not(":first").hide();
      ths
        .find(".js-catalog-sort__item")
        .click(function () {
          ths
            .find(".js-catalog-sort__item")
            .removeClass("active")
            .eq($(this).index() - 2)
            .addClass("active");
          ths
            .find(".js-catalog-sort__content")
            .hide()
            .eq($(this).index() - 2)
            .fadeIn();
        })
        .eq(0)
        .addClass("active");
    });
    if ($(window).width() < 767) {
      $(".js-sort-show").on("click", function () {
        $(".js-catalog-sort-line").slideToggle();
      });
      $(".js-sort-hide").on("click", function () {
        $(".js-catalog-sort-line").slideToggle();
      });

      $(".js-catalog-sort__item").click(function () {
        $(".js-catalog-sort-line").slideToggle();
        $(".js-catalog-sort")
          .find("js-catalog-sort__item")
          .removeClass("active")
          .eq($(this).index() - 2)
          .addClass("active");
        $(".js-catalog-sort")
          .find(".js-catalog-sort__content")
          .hide()
          .eq($(this).index() - 2)
          .fadeIn();
      });
    }
  }
  // /сортировка каталога
  // Фильтр каталога
  if ($(".js-filter").length) {
    if ($(window).width() < 767) {
      $(".js-filter-show").on("click", function () {
        $(".js-filter").slideToggle();
      });
      $(".js-filter-hide").on("click", function () {
        $(".js-filter").slideToggle();
      });

      $(".js-filter__name").on("click", function () {
        $(this).toggleClass("active");
        $(this)
          .parents(".js-filter__group")
          .find(".js-filter__content")
          .toggle();
      });

      $(".js-filter-make").on("click", function (e) {
        e.preventDefault();
        $(".js-filter").slideToggle();
      });
    }
    // /Фильтр каталога
    // ползунок
    $(".js-filter-polzunok").slider({
      min: 1000,
      max: 10000,
      values: [2000, 7000],
      range: true,
      animate: "fast",
      slide: function (event, ui) {
        $(".js-filter-polzunok-left").val(ui.values[0]);
        $(".js-filter-polzunok-right").val(ui.values[1]);
      },
    });
    $(".js-filter-polzunok-left").val(
      $(".js-filter-polzunok").slider("values", 0)
    );
    $(".js-filter-polzunok-right").val(
      $(".js-filter-polzunok").slider("values", 1)
    );
    $(".js-filter__polzunok-container input").change(function () {
      var input_left = $(".js-filter-polzunok-left")
          .val()
          .replace(/[^0-9]/g, ""),
        opt_left = $(".js-filter-polzunok").slider("option", "min"),
        where_right = $(".js-filter-polzunok").slider("values", 1),
        input_right = $(".js-filter-polzunok-right")
          .val()
          .replace(/[^0-9]/g, ""),
        opt_right = $(".js-filter-polzunok").slider("option", "max"),
        where_left = $(".js-filter-polzunok").slider("values", 0);
      if (input_left > where_right) {
        input_left = where_right;
      }
      if (input_left < opt_left) {
        input_left = opt_left;
      }
      if (input_left == "") {
        input_left = 0;
      }
      if (input_right < where_left) {
        input_right = where_left;
      }
      if (input_right > opt_right) {
        input_right = opt_right;
      }
      if (input_right == "") {
        input_right = 0;
      }
      $(".js-filter-polzunok-left").val(input_left);
      $(".js-filter-polzunok-right").val(input_right);
      if (input_left != where_left) {
        $(".js-filter-polzunok").slider("values", 0, input_left);
      }
      if (input_right != where_right) {
        $(".js-filter-polzunok").slider("values", 1, input_right);
      }
    });
    // ползунок
  }
  // Популярные бренды
  if ($(".js-brands").length) {
    var swiper = new Swiper(".js-brands", {
      navigation: {
        nextEl: ".js-brands__next",
        prevEl: ".js-brands__prev",
      },
      loop: true,
      breakpoints: {
        0: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
        1200: {
          slidesPerView: 6,
          spaceBetween: 30,
        },
      },
    });
  }
  // Популярные бренды
  // Блог
  if ($(".js-blog__items").length) {
    var swiper = new Swiper(".js-blog__items", {
      slidesPerView: "auto",
      spaceBetween: 20,
      loop: true,
    });
  }
  // /Блог
  // Видеообзоры
  if ($(".js-videos").length) {
    if ($(window).width() < 767) {
      $(".js-videos").addClass("swiper-container");
      $(".js-videos").wrapInner("<div class='swiper-wrapper'></div>");
      $(".js-videos__item").addClass("swiper-slide");
    }
    var swiper = new Swiper(".js-videos", {
      slidesPerView: 2,
      spaceBetween: 10,
    });
  }
  // /Видеообзоры
  // Информация
  if ($(".js-info__link").length) {
    $(".js-info__link").on("click", "a", function (e) {
      e.preventDefault();
      $(".js-info__link").toggle();
      $(".js-info__hide").toggle();
      $(".js-info__text").toggleClass("active");
      var h = $(".js-info__content").height();
      $(".js-info__text").animate({ height: h }, 200);
    });
    $(".js-info__hide").on("click", "a", function (e) {
      e.preventDefault();
      $(".js-info__hide").toggle();
      $(".js-info__link").toggle();
      $(".js-info__text").toggleClass("active");
      $(".js-info__text").animate({ height: "77px" }, 200);
    });
  }
  // /Информация
  /* ---------------- О КОМПАНИИ ---------------- */
  // Сотрудники
  if ($(".js-workers").length) {
    var swiper = new Swiper(".js-workers", {
      navigation: {
        nextEl: ".js-workers-button-next",
        prevEl: ".js-workers-button-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: 2,
          spaceBetween: 10,
          slidesPerColumn: 1,
          slidesPerGroup: 2,
        },
        767: {
          slidesPerView: 2, // or 'auto'
          slidesPerColumn: 2,
          slidesPerGroup: 2,
          spaceBetween: 0,
          grabCursor: true,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 30,
          slidesPerColumn: 1,
          slidesPerGroup: 4,
        },
      },
    });
  }
  // Сотрудники

  // -------------ДЕТАЛЬНАЯ----------
  // -------------/ДЕТАЛЬНАЯ----------
  // слайдер
  if ($(".js-gallery-main").length) {
    var galleryThumbs = new Swiper(".js-gallery-thumbs", {
      // loop: true,
      centeredSlides: true,
      centeredSlidesBounds: true,
      slidesPerView: 4,
      watchOverflow: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      direction: "vertical",
    });

    var galleryMain = new Swiper(".js-gallery-main", {
      // loop: true,
      watchOverflow: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      preventInteractionOnTransition: true,
      pagination: {
        el: ".js-gallery-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".js-gallery-next",
        prevEl: ".js-gallery-prev",
      },
      effect: "fade",
      fadeEffect: {
        crossFade: true,
      },
      thumbs: {
        swiper: galleryThumbs,
      },
    });
  }
  // /слайдер
  // табы
  if ($(".js-tabs-wrapper").length) {
    $(".js-tabs-wrapper").each(function () {
      let ths = $(this);
      ths.find(".js-tab-item").not(":first").hide();
      ths
        .find(".js-tab")
        .click(function () {
          ths
            .find(".js-tab")
            .removeClass("active")
            .eq($(this).index())
            .addClass("active");
          ths.find(".js-tab-item").hide().eq($(this).index()).fadeIn();
        })
        .eq(0)
        .addClass("active");
    });
  }
  // /табы
  // Характеристики
  if ($(".js-characters").length) {
    $(".js-show-characters").on("click", "a", function (e) {
      e.preventDefault();
      $(".js-show-characters").toggle();
      $(".js-hide-characters").toggle();
      $(".js-characters").toggleClass("active");
      var h = $(".js-characters-content").height();
      $(".js-characters").animate({ height: h }, 200);
    });
    $(".js-hide-characters").on("click", "a", function (e) {
      e.preventDefault();
      $(".js-hide-characters").toggle();
      $(".js-show-characters").toggle();
      $(".js-characters").toggleClass("active");
      $(".js-characters").animate({ height: "245px" }, 200);
    });
  }
  // /Информация

  // -------------КОРЗИНА----------
  if ($(window).width() < 767) {
    console.log(1);
    $(".js-cart-mobile").hide();
  }
  $(".js-cart-mobilename").on("click", function () {
    $(this).toggleClass("active");
    $(this).next(".js-cart-mobile").slideToggle();
  });
  // -------------/КОРЗИНА----------
  // -------------ЛИЧНЫЙ КАБИНЕТ----------
  // Валидация форм
  if ($("form[name='login']").length) {
    $("form[name='login']").validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 5,
        },
      },
      messages: {
        password: {
          required: "Введите пароль",
          minlength: "Слишком короткий пароль",
        },
        email: "Некорректный e-mail",
      },
      submitHandler: function (form) {
        form.submit();
      },
    });
  }
  if ($("form[name='registr']").length) {
    $("form[name='registr']").validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        name: {
          required: true,
          minlength: 2,
        },
        password: {
          required: true,
          minlength: 5,
        },
        repassword: {
          required: true,
          minlength: 5,
        },
      },
      messages: {
        password: {
          required: "Введите пароль",
          minlength: "Слишком короткий пароль",
        },
        repassword: {
          required: "Введите пароль",
          minlength: "Слишком короткий пароль",
        },
        name: {
          required: "Введите имя",
          minlength: "Слишком короткое имя",
        },
        email: "Некорректный e-mail",
      },
      submitHandler: function (form) {
        form.submit();
      },
    });
  }
  if ($("form[name='password']").length) {
    $("form[name='password']").validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
      },
      messages: {
        email: "Некорректный e-mail",
      },
      submitHandler: function (form) {
        form.submit();
      },
    });
  }
  // /Валидация форм
  // табы слева
  if ($(window).width() > 767) {
    $('.js-tab-left a[href*="#"]').click(function (e) {
      e.preventDefault();
      var href = $(this).attr("href");
      if (!$(href).hasClass("active")) {
        $(".js-tab-left-content.active").removeClass("active");
        $(href).addClass("active");
      }
    });
  } else {
    $('.js-tab-left a[href*="#"]').click(function (e) {
      e.preventDefault();
      var href = $(this).attr("href");
      $(".js-tab-left-content").hide();
      $(href).show();
      $(".profile-detail__left").hide(500);
      $(".profile-detail__rigth").show(500);
    });
    $(".js-profile-detail-back").click(function (e) {
      e.preventDefault();
      $(".js-tab-left-content").hide();
      $(".profile-detail__left").show(500);
      $(".profile-detail__rigth").hide(500);
    });
  }
  // /табы слева
  // заказ
  $(".js-order-show").on("click", function () {
    $(this).parents(".js-order").toggleClass("active");
    $(this).parents(".js-order").find(".js-order-items").slideToggle();
  });
  // -------------/ЛИЧНЫЙ КАБИНЕТ----------
  // ------------МАГАЗИНЫ----------
  // карта
  if ($("#map").length) {
    ymaps.ready(function () {
      var myMap = new ymaps.Map("map", {
          center: [54.774613, 32.055611],
          zoom: 17,
          controls: [],
        }),
        // карты
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
          '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),
        myPlacemark = new ymaps.Placemark(
          myMap.getCenter(),
          {
            hintContent: "Магазин №1",
          },
          {
            iconLayout: "default#image",
            iconImageHref: "../img/map-ballon.svg",
            iconImageSize: [93, 82],
            iconImageOffset: [-47, -82],
          }
        ),
        myPlacemarkWithContent = new ymaps.Placemark(
          [54.774732, 32.05268],
          {
            hintContent: "Магазин №2",
          },
          {
            iconLayout: "default#imageWithContent",
            iconImageHref: "../img/map-ballon.svg",
            iconImageSize: [93, 82],
            iconImageOffset: [-47, -82],
            iconContentOffset: [15, 15],
            iconContentLayout: MyIconContentLayout,
          }
        );

      myMap.geoObjects.add(myPlacemark).add(myPlacemarkWithContent);
    });
  }
  // /карта
  // реквизиты
  $(".js-requisite-title").on("click", function () {
    $(this).toggleClass("active");
    $(".js-requisite-information").slideToggle();
  });
  // реквизиты
  // ------------/МАГАЗИНЫ----------
  // ------------НОВОСТИ----------
  // фильтр
  if ($(".js-news-filter").length) {
    $(".js-category-filter a").on("click", function (e) {
      var getFilterName = $(this).attr("class");
      var isClassAll = $(this).hasClass("js-all");
      if (isClassAll != true) {
        $(
          ".js-news-list-item:not([data-category='" + getFilterName + "'])"
        ).hide();
        $(".js-news-list-item[data-category='" + getFilterName + "']").show();
      } else {
        $(".js-news-list-item").show();
      }
    });

    $(".js-year-filter a").on("click", function (e) {
      var getFilterName = $(this).attr("class");
      var isClassAll = $(this).hasClass("js-all");
      if (isClassAll != true) {
        $(".js-news-list-item:not([data-year='" + getFilterName + "'])").hide();
        $(".js-news-list-item[data-year='" + getFilterName + "']").show();
      } else {
        $(".js-news-list-item").show();
      }
    });
  }
  // ------------/НОВОСТИ----------
  // ------------/БРЕНДЫ----------
  if ($(".js-brand-detail").length) {
    var swiper = new Swiper(".js-brand-detail", {
      slidesPerView: 6, // or 'auto'
      slidesPerColumn: 3,
      slidesPerGroup: 6,
      navigation: {
        nextEl: ".js-brands-next",
        prevEl: ".js-brands-prev",
      },
      breakpoints: {
        0: {
          slidesPerView: 3,
        },
        768: {
          slidesPerView: 4,
          slidesPerGroup: 4,
          slidesPerColumn: 5,
        },
        1024: {
          slidesPerView: 5,
        },
        1200: {
          slidesPerView: 6,
        },
      },
    });
  }
  // ------------/БРЕНДЫ----------
  // ------------Спортивный клуб----------
  $("a.js-play-video").click(function (e) {
    e.preventDefault();
    $(this).hide();
    $(".js-preview").hide();
    $(".youtube-video")[0].contentWindow.postMessage(
      '{"event":"command","func":"' + "playVideo" + '","args":""}',
      "*"
    );
  });
  // ------------Тренеры----------
  // Отзывы учеников
  if ($(".js-review").length) {
    var swiper = new Swiper(".js-review", {
      autoHeight: true,
      navigation: {
        nextEl: ".js-review-next",
        prevEl: ".js-review-prev",
      },
    });
  }
  // Отзывы учеников
  // ------------/Тренеры----------
  // ------------Расписание и стоимость----------
  if ($(".js-sportgal").length) {
    var swiper = new Swiper(".js-sportgal", {
      loop: true,
      navigation: {
        nextEl: ".js-sportgal-next",
        prevEl: ".js-sportgal-prev",
      },
    });
  }
  $('[data-fancybox="gallery"]').fancybox({
    backFocus: false,
  });
  // ------------/Расписание и стоимость----------
});
